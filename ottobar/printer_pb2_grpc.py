# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import printer_pb2 as printer__pb2


class PrinterStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.GetPrintData = channel.unary_stream(
                '/print.Printer/GetPrintData',
                request_serializer=printer__pb2.PrintDataRequest.SerializeToString,
                response_deserializer=printer__pb2.PrintData.FromString,
                )


class PrinterServicer(object):
    """Missing associated documentation comment in .proto file."""

    def GetPrintData(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_PrinterServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'GetPrintData': grpc.unary_stream_rpc_method_handler(
                    servicer.GetPrintData,
                    request_deserializer=printer__pb2.PrintDataRequest.FromString,
                    response_serializer=printer__pb2.PrintData.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'print.Printer', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class Printer(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def GetPrintData(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_stream(request, target, '/print.Printer/GetPrintData',
            printer__pb2.PrintDataRequest.SerializeToString,
            printer__pb2.PrintData.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
