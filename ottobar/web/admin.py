from django.contrib import admin

from web.models import Drink, PurchasedDrink, Party, Current, Charity, UserChoices


@admin.register(Drink)
class DrinkAdmin(admin.ModelAdmin):
    pass


@admin.register(PurchasedDrink)
class PurchasedDrinkAdmin(admin.ModelAdmin):
    pass


@admin.register(Party)
class PartyAdmin(admin.ModelAdmin):
    pass


@admin.register(Current)
class CurrentAdmin(admin.ModelAdmin):
    pass


@admin.register(Charity)
class CharityAdmin(admin.ModelAdmin):
    pass


@admin.register(UserChoices)
class UserChoicesAdmin(admin.ModelAdmin):
    pass
