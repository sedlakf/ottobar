import json
import secrets
import uuid
from functools import cache
from urllib.parse import urlencode

import django.http
import jwt
import requests
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.http import require_safe, require_POST
from django.contrib.auth import login, logout

from web.models import Charity, UserChoices, PurchasedDrink, Current, Drink

GOOGLE_USER_INFO_URL = "https://www.googleapis.com/oauth2/v3/userinfo"
GOOGLE_REDIRECT_URL = f'https://{settings.PROD_HOSTNAME}/accounts/after-login/'
GOOGLE_DISCOVERY_DOCUMENT_URL = "https://accounts.google.com/.well-known/openid-configuration"

ALLOWED_DOMAINS = ["monitora.cz", "mediaboard.com", "imper.cz", "businessanimals.cz"]

SCOPES = [
    "https://www.googleapis.com/auth/userinfo.email",
    "https://www.googleapis.com/auth/userinfo.profile",
    "openid",
]


@cache
def google_oauth_metadata():
    resp = requests.get(GOOGLE_DISCOVERY_DOCUMENT_URL)
    resp.raise_for_status()
    return resp.json()


@login_required
def homepage(request):
    template = 'homepage.html'
    if request.method == 'POST':
        if 'charity_id' in request.POST:
            UserChoices.objects.filter(user=request.user).update(charity_id=request.POST['charity_id'])
            if request.headers.get('HX-Boosted'):
                template = 'components/charities.html'

    current = Current.get()
    charities = list(Charity.objects.all())
    ctx = {
        'charities': charities,
        'choice': UserChoices.objects.get_or_create(user=request.user, defaults={'charity': charities[0]})[0],
        'purchased_drinks': PurchasedDrink.objects.filter(user=request.user, party=current.party),
        'drinks': Drink.objects.filter(active=True),
    }

    return render(request, template, context=ctx)


@require_safe
def view_login(request):
    request.session['google_oauth_state'] = secrets.token_hex(16)
    params = {
        "response_type": "code",
        "client_id": settings.GOOGLE_OAUTH2_CLIENT_ID,
        "redirect_uri": GOOGLE_REDIRECT_URL,
        "scope": " ".join(SCOPES),
        "state": request.session['google_oauth_state'],
        "access_type": "offline",
        "include_granted_scopes": "true",
        "prompt": "select_account",
        "hd": "*",
    }

    query_params = urlencode(params)
    authorization_url = f"{google_oauth_metadata()['authorization_endpoint']}?{query_params}"
    return render(request, 'login.html', context={'authorization_url': authorization_url})


def view_logout(request):
    logout(request)
    return redirect(request.GET.get('next', '/'))


@require_safe
def after_login(request: django.http.HttpRequest):
    code = request.GET.get('code')
    error = request.GET.get('error')
    state = request.GET.get('state')

    if error is not None:
        raise RuntimeError(f'Error from Google: {error}')

    if code is None or state is None:
        raise ValueError('Code and state required')

    if state != request.session.pop('google_oauth_state'):
        raise ValueError('State doesn\'t match')

    data = {
        "code": code,
        "client_id": settings.GOOGLE_OAUTH2_CLIENT_ID,
        "client_secret": settings.GOOGLE_OAUTH2_CLIENT_SECRET,
        "redirect_uri": GOOGLE_REDIRECT_URL,
        "grant_type": "authorization_code",
    }

    response = requests.post(google_oauth_metadata()['token_endpoint'], data=data)
    response.raise_for_status()

    tokens = response.json()
    # We don't need to verify signature, we just got the info from Google directly
    id_token = jwt.decode(tokens['id_token'], options={'verify_signature': False})

    if id_token.get('hd') not in ALLOWED_DOMAINS or not id_token.get('email_verified'):
        return render(request, 'unauthorized.html', status=401)

    email = id_token['email']
    user, _created = User.objects.get_or_create(username=email, defaults={
        'first_name': id_token.get('given_name', ''),
        'last_name': id_token.get('family_name', ''),
        'email': email,
    })

    login(request, user)
    return redirect(request.GET.get('next', '/'))


@login_required
@require_POST
def order(request):
    ctx = {
        'drink': Drink.objects.get(pk=request.POST['drink_id']),
        'transaction_id': uuid.uuid4(),  # Prevent duplicate orders
    }
    return render(request, 'order.html', context=ctx)


@login_required
@require_POST
def order_confirmation(request):
    drink = Drink.objects.get(pk=request.POST['drink_id'])
    party = Current.get().party
    PurchasedDrink.objects.get_or_create(
        transaction_id=request.POST['transaction_id'],
        defaults={
            'user': request.user,
            'party': party,
            'price_czk': drink.price_czk,
            'headline': drink.headline,
        },
    )
    return redirect(homepage)
