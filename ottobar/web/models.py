from django.contrib.auth.models import User
from django.db import models


class Drink(models.Model):
    price_czk = models.DecimalField(max_digits=10, decimal_places=2, default=40)
    headline = models.CharField(max_length=200)

    description = models.TextField()
    active = models.BooleanField()

    def __str__(self):
        return self.headline


class Party(models.Model):
    name = models.CharField(max_length=300)
    date = models.DateTimeField()

    class Meta:
        verbose_name_plural = "parties"

    def __str__(self):
        return f'{self.name} {self.date}'


class PurchasedDrink(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    party = models.ForeignKey(Party, on_delete=models.PROTECT)
    purchase_date = models.DateTimeField(auto_now_add=True)
    transaction_id = models.UUIDField(unique=True)
    price_czk = models.DecimalField(max_digits=10, decimal_places=2, default=40)
    headline = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.user} {self.headline} {self.price_czk} Kč'


class Current(models.Model):
    party = models.ForeignKey(Party, on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = "current settings"

    @classmethod
    def get(cls):
        obj, _created = cls.objects.get_or_create(id=1)
        return obj

    def __str__(self):
        return f'Current party: {self.party}'


class Charity(models.Model):
    name = models.CharField(max_length=300)

    class Meta:
        verbose_name_plural = "charities"

    def __str__(self):
        return self.name


class UserChoices(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    charity = models.ForeignKey(Charity, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "user choices"
