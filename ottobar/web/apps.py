from django.apps import AppConfig
from django.db.backends.signals import connection_created
from django.dispatch import receiver


class WebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'web'


@receiver(connection_created)
def init_command(sender, connection, **kwargs) -> None:
    cursor = connection.cursor()
    cursor.execute('PRAGMA journal_mode=wal')
