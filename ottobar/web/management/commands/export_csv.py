import csv
import quopri
import smtplib
import sys

from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.template import loader

from web.models import PurchasedDrink, Current

MAIL_FROM = 'filip.sedlak@monitora.cz'


class Command(BaseCommand):
    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        writer = csv.writer(sys.stdout)
        writer.writerow(['E-mail', 'Jmeno', 'Kolik', 'Celkem Kc'])
        for user in User.objects.all():
            drinks = user.purchaseddrink_set.filter(party=Current.get().party)
            writer.writerow([user.email, f'{user.first_name} {user.last_name}', len(drinks), sum(d.price_czk for d in drinks)])
