import quopri
import smtplib

from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.template import loader

from web.models import PurchasedDrink, Current

MAIL_FROM = 'filip.sedlak@monitora.cz'


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("user", nargs="*")
        parser.add_argument('-p', '--only-print',action='store_true')

    def handle(self, *args, **options):
        qs = User.objects.all()
        if options["user"]:
            qs = qs.filter(username__in=options["user"])

        for user in qs:
            purchased_drinks = PurchasedDrink.objects.filter(user=user, party=Current.get().party).order_by('id')
            total_price = sum(d.price_czk for d in purchased_drinks)

            print(user.username)
            if len(purchased_drinks) == 0:
                print('No drinks')
                continue
            template = loader.get_template('mail.txt')
            msg = template.render({'purchased_drinks': purchased_drinks, 'total_price': total_price, 'user': user})

            mail = b'\r\n'.join([
                f'From: {MAIL_FROM}'.encode('ascii'),
                f'To: {user.email}'.encode('ascii'),
                b'Content-Type: text/html; charset="UTF-8"',
                b'Content-Transfer-Encoding: quoted-printable',
                'Subject: Ottobar - přehled'.encode('utf-8'),
                b'',
                quopri.encodestring(b'<pre>\n' + msg.encode('utf-8') + b'\n</pre>\n'),
                ])

            if options['only_print']:
                print(msg)
            else:
                with smtplib.SMTP_SSL('email-smtp.eu-central-1.amazonaws.com') as smtp:
                    smtp.login(settings.SES_SMTP_USERNAME, settings.SES_SMTP_PASSWORD)
                    smtp.sendmail(MAIL_FROM, user.email, mail)
