from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional

DESCRIPTOR: _descriptor.FileDescriptor

class PrintDataRequest(_message.Message):
    __slots__ = ("last_transaction_id_seen",)
    LAST_TRANSACTION_ID_SEEN_FIELD_NUMBER: _ClassVar[int]
    last_transaction_id_seen: int
    def __init__(self, last_transaction_id_seen: _Optional[int] = ...) -> None: ...

class PrintData(_message.Message):
    __slots__ = ("drink_name", "customer", "transaction_id")
    DRINK_NAME_FIELD_NUMBER: _ClassVar[int]
    CUSTOMER_FIELD_NUMBER: _ClassVar[int]
    TRANSACTION_ID_FIELD_NUMBER: _ClassVar[int]
    drink_name: str
    customer: str
    transaction_id: int
    def __init__(self, drink_name: _Optional[str] = ..., customer: _Optional[str] = ..., transaction_id: _Optional[int] = ...) -> None: ...
