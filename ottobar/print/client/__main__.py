import base64
import logging
import sqlite3
import unicodedata
import grpc

import printer_pb2
import printer_pb2_grpc
from . import client_secrets


logging.basicConfig(level=logging.INFO)


class BasicAuthPlugin(grpc.AuthMetadataPlugin):
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __call__(self, context, callback):
        secret = base64.b64encode(f'{self.username}:{self.password}'.encode('utf-8')).decode('ascii')
        metadata = (("authorization", f"Basic {secret}"),)
        callback(metadata, None)


def ascii_fold(text: str) -> str:
    """Replace accented characters to their ASCII equivalent."""

    return unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode('ascii')


print_template = \
'''{drink_name}

{customer}

------------------------------

'''

credentials = grpc.composite_channel_credentials(
    grpc.ssl_channel_credentials(),
    grpc.metadata_call_credentials(BasicAuthPlugin(*client_secrets.PRINTER_SERVER_AUTH)))

db = sqlite3.connect('client.sqlite3')
cursor = db.cursor()
cursor.execute('CREATE TABLE IF NOT EXISTS transactions (id INTEGER PRIMARY KEY, drink_name TEXT, customer TEXT)')
db.commit()

cursor.execute('SELECT MAX(id) FROM transactions')
last_transaction_id_seen = cursor.fetchone()[0] or 0

logging.info('Starting client process, last_transaction_id_seen: "%s"', last_transaction_id_seen)
with grpc.secure_channel("ottobar.alcotech.cz:443", credentials) as channel:
    request = printer_pb2.PrintDataRequest(last_transaction_id_seen=last_transaction_id_seen)
    server = printer_pb2_grpc.PrinterStub(channel)
    logging.info('Connected to server, requesting print data in loop...')

    for print_data in server.GetPrintData(request):
        logging.info('Sending data with transaction ID "%s" to printer.', print_data.transaction_id)

        print_string = print_template.format(
            drink_name=print_data.drink_name,
            customer=ascii_fold(print_data.customer))
        with open('/dev/usb/lp0', 'w') as printer:
            printer.write(print_string)
        logging.info('Printed (ID "%s"): \n%s', print_data.transaction_id, print_string)

        cursor.execute('INSERT INTO transactions (id, drink_name, customer) VALUES (?, ?, ?)',
                       (print_data.transaction_id, print_data.drink_name, print_data.customer))
        db.commit()
