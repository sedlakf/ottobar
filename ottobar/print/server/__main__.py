import logging
import os

import django


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ottobar.settings')
    django.setup()

    from .server import serve

    serve('127.0.0.1:50100')


if __name__ == '__main__':
    main()
