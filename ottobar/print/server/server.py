import logging
import time
from concurrent.futures import ThreadPoolExecutor

import grpc

from web.models import PurchasedDrink
import printer_pb2_grpc, printer_pb2


def create_print_data(purchased_drink: PurchasedDrink) -> printer_pb2.PrintData:
    user = purchased_drink.user
    if user.first_name.strip() and user.last_name.strip():
        customer_name = f'{user.first_name} {user.last_name}'
    elif '@' in user.username:
        customer_name = user.username.split('@')[0]
    else:
        customer_name = user.username
    return printer_pb2.PrintData(drink_name=purchased_drink.headline, customer=customer_name,
                                 transaction_id=purchased_drink.id)


class Printer(printer_pb2_grpc.PrinterServicer):
    def GetPrintData(self, request: printer_pb2.PrintDataRequest, context):
        last_transaction_id = request.last_transaction_id_seen
        logging.info('last_transaction_id: %s', last_transaction_id)
        while context.is_active():
            for purchased_drink in (
                    PurchasedDrink.objects.filter(id__gt=last_transaction_id).order_by('id')
            ):
                data = create_print_data(purchased_drink)
                logging.info('sending drink: %s', data)
                yield data

                last_transaction_id = purchased_drink.id

            time.sleep(0.5)


def serve(address: str) -> None:
    server = grpc.server(ThreadPoolExecutor(max_workers=2))
    printer_pb2_grpc.add_PrinterServicer_to_server(Printer(), server)
    server.add_insecure_port(address)
    server.start()
    logging.info("Server serving at %s", address)
    server.wait_for_termination()
